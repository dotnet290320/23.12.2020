CREATE OR REPLACE FUNCTION a_sp_max(x integer, y integer, z integer)
returns integer AS
    $$
    BEGIN
        IF x > y AND x > z THEN
            RETURN x;
        ELSEIF y > z THEN
            RETURN y;
        ELSE
            RETURN z;
        END IF;
END;
$$ language plpgsql;

select * from a_sp_max(5, 10, 17);

CREATE OR REPLACE FUNCTION a_sp_max_case(x integer, y integer, z integer)
returns integer AS
    $$
    BEGIN
        CASE
            WHEN x > y AND x > z THEN
                RETURN x;
            WHEN y > z THEN
                RETURN y;
        ELSE
            RETURN z;
        END CASE;
END;
$$ language plpgsql;

select * from a_sp_max_case(5, 100, 17);

CREATE OR REPLACE FUNCTION a_sp_movie_id_1_or_2(_id_type text)
returns TABLE(ids bigint, title text) AS
    $$
    BEGIN
        return query
        SELECT CASE WHEN _id_type ='M' THEN movies.id else movies.country_id END,
               movies.title from movies;
    END;
$$ language plpgsql;

SELECT * FROM a_sp_movie_id_1_or_2('C')

-- 1. get two numbers a,b (int) and operation (text) returns float
        --if operation '+' return a+b if '-' a-b '*' a*b '/' a/b
-- 2. solve 1 using case
-- 1. get two numbers a,b (int) and operation (text) returns float
        --if operation '+' return a+b if '-' a-b '*' a*b '/' a/b
-- 2. solve 1 using case
-- 3. return movie price as is or pow2 depends on sent boolean argument
CREATE OR REPLACE FUNCTION a_sp_return_price_or_sqr(_pow boolean)
returns TABLE(_title text, _price double precision) AS
    $$
    BEGIN
        return query
        SELECT movies.title,
               CASE WHEN NOT _pow THEN movies.price else pow(movies.price, 2) END
                from movies;
    END;
$$ language plpgsql;

SELECT * FROM a_sp_return_price_or_sqr(_pow => false)

CREATE OR REPLACE FUNCTION a_sp_get_randoms(_max integer) -- 1-max
returns integer AS
    $$
    BEGIN
        return (random() * (_max - 1)) + 1;
    END;
$$ language plpgsql;

select * from a_sp_get_randoms(2);

CREATE OR REPLACE FUNCTION a_sp_loop1() -- 1-max
returns integer AS
    $$
    declare
        sum int := 0;
    BEGIN
        FOR i IN 1..(select count(*) from country)
            loop
                sum := sum + (select id from country where id=i);
            end loop;
        return sum;
    END;
$$ language plpgsql;

select * from a_sp_loop1();

create table grades
(
	id bigserial
		constraint grades_pk
			primary key,
	class_id bigint not null,
	student_id bigint not null,
	grade double precision default 0 not null
);


CREATE OR REPLACE FUNCTION a_sp_populate_grade(_classes int, _students int) -- 1-max
returns integer AS
    $$
    declare
        counter int := 0;
        _grade double precision := 0;
    BEGIN
        FOR i IN 1.._classes
            loop
                FOR j IN 1.._students -- for (int j = 1; j <= _students; j++)
                loop
                    counter := counter + 1;
                    _grade = random() * 100;
                    INSERT INTO grades(class_id, student_id, grade) VALUES
                        (i, counter, _grade);
                    end loop;
            end loop;
        return counter;
    END;
$$ language plpgsql;

drop table grades;

select * from a_sp_populate_grade(5, 30);

--delete from grades
--where class_id = 1 and student_id = 2;

-------------------------- best grade for each class
select * from (
select *,
       row_number() over (partition by class_id order by class_id, grade desc) row_num
from grades
order by class_id, grade desc ) c
where c.row_num = 1;

-------------------------- second best grade for each class
select * from (
select *,
       row_number() over (partition by class_id order by class_id, grade desc) row_num
from grades
order by class_id, grade desc ) c
where c.row_num = 2;

-- use partitioning and find if there are 2 students with the same grade ?
select * from (
select *,
       row_number() over (partition by grade::int) row_num
from grades ) c
where c.row_num = 2;

----------------------------- each student how many students in class
select *, (grade - class_avg) diff from (
select student_id, class_id, grade,
       count(*) over (partition by class_id) students_in_class,
       avg(grade) over (partition by class_id) class_avg
from grades ) c



-- Homework!
-- for each row [student] present the max grade of the class
-- create sp when given a student id --> present student_it, class_id, grade, class_avg, diff, max grde, min grade



